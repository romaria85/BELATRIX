package org.belatrix.component.listener;

import java.util.Map;
import javax.validation.Valid;
import org.belatrix.component.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

@Component
@RabbitListener(queues = "queue", containerFactory = "listenerContainerFactory")
public class JobLauncherQueueListener extends AbstractQueueListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobLauncherQueueListener.class);

	@Autowired
	Processor processor;
	
	@Transactional
	@RabbitHandler
	public void onMessage(@Valid @Payload Object message, @Headers Map<String, Object> headers) {
		try {
			super.onMessage(message, headers);
			
			if(!ObjectUtils.isEmpty(this.mapMessage)){
				processor.process(this.mapMessage);
			}
		
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
