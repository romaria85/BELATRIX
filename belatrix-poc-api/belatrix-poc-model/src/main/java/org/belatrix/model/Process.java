package org.belatrix.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author j@lor
 *
 */
@Document
public class Process extends AbstractDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7737648919551799435L;
	private String name;
	private String type;
	private String nameAlgorithm;
	private String gridSize;
	
	@DBRef
	private List<Execution> executions;
	
	@DBRef
	private List<Pattern>   patterns;
	
	private Map<String, Object> executor;
	private Map<String, Object> listener;
	private Map<String, String> settings;

	private String status;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Pattern> getPatterns() {
		return patterns;
	}

	public void setPatterns(List<Pattern> patterns) {
		this.patterns = patterns;
	}

	public List<Execution> getExecutions() {
		return executions;
	}
	
	public void setExecutions(List<Execution> executions) {
		this.executions = executions;
	}
	
	public Map<String, Object> getExecutor() {
		return executor;
	}
	
	public void setExecutor(Map<String, Object> executor) {
		this.executor = executor;
	}

	public Map<String, String> getSettings() {
		return settings;
	}

	public void setSettings(Map<String, String> settings) {
		this.settings = settings;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<String, Object> getListener() {
		return listener;
	}

	public void setListener(Map<String, Object> listener) {
		this.listener = listener;
	}

	public String getNameAlgorithm() {
		return nameAlgorithm;
	}

	public void setNameAlgorithm(String nameAlgorithm) {
		this.nameAlgorithm = nameAlgorithm;
	}

	public String getGridSize() {
		return gridSize;
	}

	public void setGridSize(String gridSize) {
		this.gridSize = gridSize;
	}
	
	
}
