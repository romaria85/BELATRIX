package org.belatrix.component;

import java.util.Map;

import org.belatrix.canonical.QSIGenericMessage;

/**
 * It defines a common interface to process all messages.
 * 
 * @author jalor
 *
 */
public interface Processor {


	/**
	 * 
	 * @param message
	 * @return
	 */
	QSIGenericMessage process(Map<String, Object> message);
	
	
}
