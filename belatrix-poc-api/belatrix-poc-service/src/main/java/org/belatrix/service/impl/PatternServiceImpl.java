package org.belatrix.service.impl;

import org.belatrix.model.Pattern;
import org.belatrix.repository.PatternRepository;
import org.belatrix.service.PatternService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jalor
 *
 */
@Service
public class PatternServiceImpl implements PatternService {
	
	@Autowired
	private PatternRepository repo;

	@Override
	public Pattern findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public Pattern save(Pattern entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(Pattern entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		return repo.exists(id);
	}

}
