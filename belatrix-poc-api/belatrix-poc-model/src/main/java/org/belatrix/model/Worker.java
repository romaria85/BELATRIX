package org.belatrix.model;

import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Worker extends AbstractDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8153163162285240656L;
	
	private String name;
	private String type;
	private Map<String, Object> parameters;
	private String status;
	private String order;
	private String idProcessLogger;
	
	@DBRef
	private ProcessLogger processLogger;
	
	@DBRef
	private List<Task> tasks;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getIdProcessLogger() {
		return idProcessLogger;
	}

	public void setIdProcessLogger(String idProcessLogger) {
		this.idProcessLogger = idProcessLogger;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public ProcessLogger getProcessLogger() {
		return processLogger;
	}

	public void setProcessLogger(ProcessLogger processLogger) {
		this.processLogger = processLogger;
	}	
	
	
}
