package org.belatrix.service.impl;

import org.belatrix.model.ProcessLogger;
import org.belatrix.repository.ProcessLoggerRepository;
import org.belatrix.service.ProcessLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author jalor
 *
 */
@Service
public class ProcessLoggerServiceImpl implements ProcessLoggerService {
	
	@Autowired
	private ProcessLoggerRepository repo;
	
	@Override
	public ProcessLogger findById(String id) {
		return repo.findOne(id);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public ProcessLogger save(ProcessLogger entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(ProcessLogger entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		
		return repo.exists(id);
	}

	
	
}
