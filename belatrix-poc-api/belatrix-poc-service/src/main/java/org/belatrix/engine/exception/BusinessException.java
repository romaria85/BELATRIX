package org.belatrix.engine.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author jalor
 *
 */
public class BusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String message;
	private String type;
	private List<ErrorBean> errorList;

	public BusinessException(String message) {
		
		super(message);
		this.message = message;
	}

	public BusinessException(String code,String message) {		
		
		super(message);
		this.code = code;
		this.message = message;
	}	
	
	public BusinessException(String code,String message, String type) {		
		super(message);
		this.code = code;
		this.message = message;
		this.setType(type);
	}

	public BusinessException(String code, String message, List<ErrorBean> errorList) {
		super(message);
		this.code = code;
		this.message = message;
		this.errorList = errorList;
	}

	public BusinessException(HttpStatus status, String message){
		super(message);
		this.code = status.value() + "";
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {		
		this.message = message;
	}

	public List<ErrorBean> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<ErrorBean> errorList) {
		this.errorList = errorList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

