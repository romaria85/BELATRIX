package org.belatrix.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.belatrix.canonical.QSIGenericMessage;
import org.belatrix.model.Execution;
import org.springframework.stereotype.Component;


/**
 * Implementation of the enricher Java Object, fill fields using Script in
 * JS.
 * 
 * @author jalor
 *
 */
@Component("transformerJS")
public class ScriptEngineEnricher implements EnricherStrategy {

	private static final Log LOG = LogFactory.getLog(ScriptEngineEnricher.class);

	@Override
	public QSIGenericMessage enrich(Execution execution, QSIGenericMessage message) {
		return null;
	}

}
