package org.belatrix.repository;

import org.belatrix.model.Pattern;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface PatternRepository extends MongoRepository<Pattern, String> {

}
