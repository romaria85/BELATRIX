package org.belatrix.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

/**
 * 
 * @author j@lor
 *
 */
public class AbstractDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1041323633375778571L;
	@Id
	private String id;
	
	
	/**
	 * Returns the identifier of the document.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (this.id == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
			return false;
		}

		AbstractDocument that = (AbstractDocument) obj;

		return this.id.equals(that.getId());
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}
}
