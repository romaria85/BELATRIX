package org.belatrix.service;

import org.belatrix.model.Pattern;

/**
 * 
 * @author jalor
 *
 */
public interface PatternService extends GenericService<Pattern, String> {

}
