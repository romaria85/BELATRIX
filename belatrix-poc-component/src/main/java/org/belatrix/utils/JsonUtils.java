package org.belatrix.utils;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dslplatform.json.DslJson;
import com.dslplatform.json.JsonWriter;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JsonUtils {

	private static final Log logger = LogFactory.getLog(JsonUtils.class);

	private static final String BLANCK_CHARACTER = "";

	private static final String UNICODE_CHARACTER_EXPRESSION = "\\r|\\t|\\n";
	
	private JsonUtils() {
		
	}
	
	public static String serializeToString(Object value) {
		DslJson dslJson = new DslJson<>();			
		JsonWriter writer = dslJson.newWriter();
		
		try {
			dslJson.serialize(writer,value);
			return writer.toString();
		} catch (IOException e) {	
			return "{}";
		}	
	}

	public static <T> T deserialize(String value, Class<?> clazz) {
		DslJson<T> dslJson = new DslJson<T>();			
		JsonWriter writer = dslJson.newWriter();		
		try {
			return (T) dslJson.deserialize(clazz, value.getBytes(),	value.getBytes().length);
		} catch (IOException e) {	
			return null;
		}	
	}

	public static List<Object> jsonToList(String json){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json.replaceAll(UNICODE_CHARACTER_EXPRESSION, BLANCK_CHARACTER), List.class);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String blobToString(Blob blob) {
		byte[] bytes = null;
		try {
			if (blob.length() > 0){
				bytes = blob.getBytes(1, (int) blob.length());
			}
		}
		catch (SQLException e) {
			logger.error(e.getMessage());
		}
		String string = new String(bytes);
		if (!string.startsWith("{")) {
			string = string.substring(string.indexOf('{'), string.length());
		}
		return string;
	}

	public static Blob stringToBlob(String string) {
		Blob blob = null;
		byte[] bytes = string.getBytes();
		try {
			blob = new SerialBlob(bytes);
		}
		catch (Exception e) {
			logger.error(e.getMessage());
		}
		return blob;

	}
	
	
	public static boolean hasJsonMapValue(String value){
		return value!=null?value.startsWith("{") && value.endsWith("}"):false;
		
	}
	
	public static boolean hasJsonListValue(String value){
		return value!=null?value.startsWith("[") && value.endsWith("]"):false;
	}
	
	public static Object getObjValue(Object value) {
		Object newObj = null;
		if (value instanceof String) {
			String strValue = (String) value;
			if (JsonUtils.hasJsonMapValue(strValue)) {
				newObj = JsonUtils.deserialize(strValue, HashMap.class);
			} else if (JsonUtils.hasJsonListValue(strValue)) {
				newObj = JsonUtils.jsonToList(strValue);
			}else{
				newObj = value;
			}
		} else {
			newObj = value;
		}
		return newObj;
	}

}
