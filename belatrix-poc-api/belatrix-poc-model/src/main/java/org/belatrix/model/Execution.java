package org.belatrix.model;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Execution extends AbstractDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8153163162285240656L;
	
	private String name;
	private String type;
	private Map<String, Object> executor;
	private String status;
	private String order;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getExecutor() {
		return executor;
	}

	public void setExecutor(Map<String, Object> executor) {
		this.executor = executor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}	
	
}
