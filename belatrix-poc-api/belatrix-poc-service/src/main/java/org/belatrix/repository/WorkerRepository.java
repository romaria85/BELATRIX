package org.belatrix.repository;

import java.util.List;

import org.belatrix.model.Worker;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface WorkerRepository extends MongoRepository<Worker, String> {
	
	List<Worker> findByName(String name);

}
