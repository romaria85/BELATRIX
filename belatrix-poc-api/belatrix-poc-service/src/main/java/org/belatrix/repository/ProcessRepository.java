package org.belatrix.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.belatrix.model.Process;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface ProcessRepository extends MongoRepository<Process, String> {
	
	List<Process> findByName(String name);

}
