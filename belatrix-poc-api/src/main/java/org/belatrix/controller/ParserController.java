package org.belatrix.controller;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.belatrix.engine.GenericPartitioner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author jalor
 *
 */
@RestController
public class ParserController {
	
	private static final Log LOGGER = LogFactory.getLog(ParserController.class);
	
	@Autowired
	GenericPartitioner genericPartitioner;
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/file", method = RequestMethod.GET)
	@ResponseBody
    public String getLocatedOnDriveByRequestParam(@RequestParam("route") String route , @RequestParam("patternCode") String patternCode) {
		
		Assert.notNull(route);
		Assert.notNull(patternCode);
		genericPartitioner.processPartition(route, patternCode);
		LOGGER.debug("asynchronous call for example socket for refresh progress of process");
        return "just test!!!";
    }
	 
}
