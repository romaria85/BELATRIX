package org.belatrix.service;

import org.belatrix.model.ProcessLogger;

/**
 * 
 * @author jalor
 *
 */
public interface ProcessLoggerService extends GenericService<ProcessLogger, String> {

}
