package org.belatrix.canonical;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.util.ObjectUtils;

public class QSIGenericMessage implements QSIMessage<QSIGenericMessage>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7877175675188454586L;

	private QSIMessageHeaders headers;
	
	private Map<String,Object> gridPayload;
	
	private Object payload;

	public QSIGenericMessage() {
		this(new ConcurrentHashMap<String,Object>(),null);
	}
	
	public QSIGenericMessage(Map<String, Object> headers) {
		this(headers, null);
	}


	public QSIGenericMessage(Map<String, Object> headers, Map<String,Object> gridPayload) {
		if (headers == null) {
			headers = new ConcurrentHashMap<String, Object>();
		} else {
			headers = new ConcurrentHashMap<String, Object>(headers);
		}
		this.headers = new QSIMessageHeaders(headers);
		if(!Objects.isNull(gridPayload)){
			this.gridPayload=new LinkedCaseInsensitiveMap(gridPayload.size());
			this.gridPayload.putAll(gridPayload);
		}else{
			this.gridPayload=new LinkedCaseInsensitiveMap<>();
		}
	}
	
	public QSIMessageHeaders getHeaders() {
		return headers;
	}

	

	public Map<String, Object> getGridPayload() {
		return gridPayload;
	}	
	
	public String toString() {
		return "[Payload=" + this.gridPayload + "][Headers=" + this.headers + "]";
	}

	public int hashCode() {
		return this.headers.hashCode() * 23 + ObjectUtils.nullSafeHashCode(this.gridPayload);
	}

	public void setGridPayload(Map<String, Object> gridPayload) {
		this.gridPayload = gridPayload;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public void setHeaders(QSIMessageHeaders headers) {
		this.headers = headers;
	}
}
