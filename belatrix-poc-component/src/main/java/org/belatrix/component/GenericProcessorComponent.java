package org.belatrix.component;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.belatrix.canonical.QSIGenericMessage;
import org.belatrix.constant.Constant;
import org.belatrix.model.ProcessLogger;
import org.belatrix.service.ProcessLoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * 
 * @author jalor
 *
 */
@Component
@Scope("prototype")
public class GenericProcessorComponent implements Processor {
	
	private static final Log LOG = LogFactory.getLog(GenericProcessorComponent.class);
	
	@Autowired
	protected ProcessLoggerService processLoggerServiceImpl;

	@Override
	public QSIGenericMessage process(Map<String, Object> messageMap) {
		
		/**
		 *  create QSIGenericMessages 
		 */
		ProcessLogger processLogger = processLoggerServiceImpl.findById(messageMap.get(Constant._ID).toString());
		LOG.info("current process logger id :" + processLogger.getId() + " to parse");
		
		/**
		 *   code hasn't yet been  implemented!
		 */
		return null;
	}


	
}
