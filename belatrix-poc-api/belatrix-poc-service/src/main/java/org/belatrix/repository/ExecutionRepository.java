package org.belatrix.repository;

import org.belatrix.model.Execution;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface ExecutionRepository extends MongoRepository<Execution, String> {

}
