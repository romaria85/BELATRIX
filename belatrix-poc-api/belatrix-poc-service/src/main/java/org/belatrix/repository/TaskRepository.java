package org.belatrix.repository;

import java.util.List;
import org.belatrix.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface TaskRepository extends MongoRepository<Task, String> {
	
}
