package org.belatrix.component;

import org.belatrix.canonical.QSIGenericMessage;
import org.belatrix.model.Execution;

/**
 * It defines a common interface to the algorithms of validation it supports.
 * 
 * @author jalor
 *
 */
public interface ValidatorStrategy {


	/**
	 * @param validation
	 * @param message
	 * @return
	 */
	boolean validate(Execution execution, QSIGenericMessage message);
}
