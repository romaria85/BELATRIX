package org.belatrix.service;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.collections.MapUtils;
import org.belatrix.model.Process;
import org.belatrix.repository.ProcessRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;


/**
 * 
 * @author j@lor
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/application-context-test.xml")
public class AbstractServiceTest {
	
	public static final String THREADS_X_SLAVES ="20"; // OR  partition = 1, 2 or more partition slaves
	public static final String GRID_SIZE = "5000"; // OR  partition = 1, 2 or more partition slaves
	private static final String NUM_SLAVES = "10";
	private static final String HYBRID_CONFIGURED = "True";
	public static final boolean GRID_SIZE_CONFIGURED = true;	
	public static final String ACCOUNT = "team12c"; //OKLWNPERF team12c papyrmd
	public static final String ACCOUNT_ALL = "-1";
	private static final String URL_POPHEALTH = "http://pophealth:3000/api/queries/qsi_calculation"; // http://pophealth:3000/api/queries/qsi_calculation
	private static final String USERNAME="qsi";
	private static final String PASSWORD="qsipophealth";
	private static final String QSI_CQM = "qsi_cqm";
	private static final String MAIL_TO = "luis.correa@qualifacts.com,david.motta@qualifacts.com,jorge.alor@qualifacts.com";	
	private static final LinkedList<String> RAW_JOBS_ORDERED = getRawJobsOrderedToExecute();
	private static final boolean CONFIGURE_SCHEDULE_JOB = false;
	private static final String ENV = "CERT";
	
	
	@Before
	public void methodTest() {
		System.out.println("");
	}
	
	@After
	public void finish() {
		System.gc();
	}
	
	public static LinkedList<String> getRawJobsOrderedToExecute(){
		LinkedList<String> rawJobsOrdered = new LinkedList();
		rawJobsOrdered.add("rawOrganization");
		rawJobsOrdered.add("rawPractitioner");
		rawJobsOrdered.add("rawPractitionerChar");
		rawJobsOrdered.add("rawPatientChar");
		rawJobsOrdered.add("rawAddress");
		rawJobsOrdered.add("rawPatient");
		rawJobsOrdered.add("rawTelecom");
		rawJobsOrdered.add("rawEpisode");
		rawJobsOrdered.add("rawPayer");
		rawJobsOrdered.add("rawPhq9");
		rawJobsOrdered.add("rawModReferral");
		rawJobsOrdered.add("rawMedicationDocumented");
		rawJobsOrdered.add("rawExternalProcedure");
		rawJobsOrdered.add("rawInpatientAdmission");
		rawJobsOrdered.add("rawCommunication");
		rawJobsOrdered.add("rawImmune");
		rawJobsOrdered.add("rawTobacco");
		rawJobsOrdered.add("rawLabResult");
		rawJobsOrdered.add("rawMedication");
		rawJobsOrdered.add("rawSvcDoc");
		rawJobsOrdered.add("rawVital");
		rawJobsOrdered.add("rawEnc");
		rawJobsOrdered.add("rawDiagnosis");
		rawJobsOrdered.add("rawbusinessRules");
		return rawJobsOrdered;
	}
	
//	@Autowired
//	protected JobInstanceService service;
//
//	@Autowired
//	protected JobService jobService;
//	
//	@Autowired
//	protected ExecutionService executionService;
//	
//	@Autowired
//	protected CommunicationService communicationService;
//	
//	@Autowired
//	protected StepService stepService;
//	
//	@Autowired
//	protected ProcessService processService;
//	
//	@Autowired
//	protected GenericMongoDAO genericMongoDAO;
//	
//	@After
//	public void finish() {
//		System.gc();
//	}
//	
//	public void deleteThreeObject(String instanceName){
//		JobInstance jobInstance = service.findJobInstanceActiveByName(instanceName,ACCOUNT_ALL!=null?ACCOUNT_ALL:ACCOUNT);	
//		if(null!=jobInstance){
//			Map<String,Object> delete = new HashMap<String,Object>();
//			delete.put("collectionName", "jobInstance");
//			delete.put("_id", jobInstance.getId());
//			genericMongoDAO.delete(delete);
//		}
//	}
	
	public String getScript(String scriptName) throws IOException{
		StringBuilder result = new StringBuilder("");
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("scripts/"+scriptName).getFile());
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}
			scanner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result.toString();
	}
	
	
//	public JobInstance saveJobInstance(Job jobCqmRaw, String jobName, int order){
//		JobInstance patientInstance = new JobInstance();
//		patientInstance.setJob(jobCqmRaw);
//		patientInstance.setName(jobName);
//		patientInstance.setOrder(order);
//		patientInstance.setStatus("ACTIVE");
//		patientInstance.setAccount(ACCOUNT_ALL!=null?ACCOUNT_ALL:ACCOUNT);
//		return service.save(patientInstance);
//	}
//	
//	public Job saveJob(Step master, Step slave){
//		Job jobCqmRaw = new Job();
//		jobCqmRaw.setSteps(new ArrayList<Step>());
//		jobCqmRaw.getSteps().add(master);
//		jobCqmRaw.getSteps().add(slave);		
//		return jobService.save(jobCqmRaw); 
//	}
//	
//	public Step saveStep(String name, int order,com.qualifacts.carelogic.batch.model.Process ... process ){
//		Step slave = new Step();
//		slave.setName(name);
//		slave.setOrder(order);
//		slave.setProcesses(new ArrayList<com.qualifacts.carelogic.batch.model.Process>());
//		for (com.qualifacts.carelogic.batch.model.Process process2 : process) {
//			slave.getProcesses().add(process2);
//		}
//		slave.setStatus("ACTIVE");
//		return stepService.save(slave);
//	}
//	
//	public com.qualifacts.carelogic.batch.model.Process saveProcess(String processName, String processType,Communication comIn,Communication comOut,String executorType, String executorName, String executorScript, boolean hasGridSize, boolean hasFetchSize, Map<String,Integer> parameters,Boolean isWriterMassive, Execution ... executions){
//		return this.saveProcess( processName,  processType, comIn, comOut, executorType,  executorName,  executorScript,null,  hasGridSize,  hasFetchSize, parameters,isWriterMassive,  executions);
//	}
//	
//	public com.qualifacts.carelogic.batch.model.Process saveProcess(String processName, String processType,Communication comIn,Communication comOut,String executorType, String executorName, String executorScript, Object preExecutor, boolean hasGridSize, boolean hasFetchSize, Map<String,Integer> parameters,Boolean isWriterMassive, Execution ... executions){
//		com.qualifacts.carelogic.batch.model.Process processPartitioner = new com.qualifacts.carelogic.batch.model.Process();
//		if(comIn!=null){
//			processPartitioner.setInput(comIn);
//		}
//		if(comOut!=null){
//			processPartitioner.setOutput(comOut);
//		}
//		if(executorType!=null /*&& (executions==null || executions.length==0)*/){
//			Map<String,Object> partitionerExecutor = new HashMap<String,Object>();
//			partitionerExecutor.put("type", executorType);
//			partitionerExecutor.put("name", executorName);
//			partitionerExecutor.put("script", executorScript);
//			partitionerExecutor.put("parameters", parameters);
//			if( !ObjectUtils.isEmpty(preExecutor)){
//				Map<String,Integer> preExecutorParameters = new HashMap<>();
//				preExecutorParameters.put("originCodes", Types.ARRAY);
//				partitionerExecutor.put("preExecutorScript", preExecutor);
//				partitionerExecutor.put("preExecutorParameters", preExecutorParameters);
//			}
//			processPartitioner.setExecutor(partitionerExecutor);
//		}else{
//			Map<String,Object> partitionerExecutor = new HashMap<String,Object>();
//			partitionerExecutor.put("type", executorType);
//			partitionerExecutor.put("name", executorName);
//			partitionerExecutor.put("parameters", parameters);
//			processPartitioner.setExecutor(partitionerExecutor);
//		}
//		processPartitioner.setName(processName);
//		processPartitioner.setType(processType);
//		processPartitioner.setStatus("ACTIVE");
//		
//		Map<String,String> partitionerSettings = new HashMap<String,String>();
//		if(hasGridSize && "GenericPartitioner".equals(processName)){
//			partitionerSettings.put("gridSize", GRID_SIZE); // OR  partition = 1, 2 or more partition slaves
//			partitionerSettings.put("algorithmName", "gridAlgorithm"); // OR partitionAlgorithm
//			if(new Boolean(HYBRID_CONFIGURED)){
//				partitionerSettings.put("hybrid", HYBRID_CONFIGURED);
//				partitionerSettings.put("threadSize", NUM_SLAVES);
//			}
//		} else if ("GenericPartitioner".equals(processName)){
//			partitionerSettings.put("partition", THREADS_X_SLAVES); // OR  partition = 1, 2 or more partition slaves
//			partitionerSettings.put("algorithmName", "partitionAlgorithm"); // OR partitionAlgorithm
//		}else if("GenericWriterComponent".equals(processName)&&isWriterMassive){
//			partitionerSettings.put("isWriterMassive", "true");
//			
//		}
//		//getSettings().get(ALGORITHM_NAME):
//		
//		processPartitioner.setSettings(partitionerSettings);
//		
//		processPartitioner.setExecutions(new ArrayList<Execution>());
//		for (Execution execution : executions) {
//			processPartitioner.getExecutions().add(execution);
//		}
//		
//		return processService.save(processPartitioner);
//	}
//	
//	public Communication saveCommunication(String dataSourceName){
//		Communication partitionerComm = new Communication();
//		partitionerComm.setConnectionType("DB");
//		partitionerComm.setStatus(true);
//		partitionerComm.setName(dataSourceName);
//		return communicationService.save(partitionerComm);
//	}
//	
//	public Execution saveExecution(String type,String scriptName,Map<String,Object> executor,String order){
//		Execution filter = new Execution();
//		filter.setName(scriptName);
//		filter.setType(type);
//		filter.setStatus(true);
//		filter.setExecutor(executor);
//		filter.setOrder(order);
//		return executionService.save(filter);
//	}
	public Map<String,Object> getExecutor(String scriptEngine, String scriptName, Object script, Map<String,Object> parameters){
		return this.getExecutor(scriptEngine, scriptName, script, parameters,null,false);
	}
	
	public Map<String,Object> getExecutor(String scriptEngine, String scriptName, Object script, Map<String,Object> parameters,boolean queryInCache){
		return this.getExecutor(scriptEngine, scriptName, script, parameters,null,queryInCache);
	}
	
	public Map<String,Object> getExecutor(String scriptEngine, String scriptName, Object script, Map<String,Object> parameters,String resultType,boolean queryInCache){
		Map<String,Object> patientFilter = new HashMap<String,Object>();
		patientFilter.put("type", scriptEngine);
		patientFilter.put("name", scriptName);
		patientFilter.put("script", script);
		if(null!=parameters){
			patientFilter.put("parameters", parameters);
		}
		patientFilter.put("resultType", null!=resultType?resultType:"List");
		patientFilter.put("queryInCache", queryInCache);	
		return patientFilter;
	}
	
//	public void saveBatchRawJob(String folder, String jobInstanceName, boolean hasEnricher,boolean hasTransformer,boolean hasFilter, boolean isCompositeWriter, List<Execution> writers, Map<String,Integer> partitionedParams,Map<String,Integer> parameters,List<Execution> executionsCustom) throws IOException {
//		saveBatchRawJob( folder,  jobInstanceName,  hasEnricher, hasTransformer, hasFilter,  isCompositeWriter,writers,partitionedParams,parameters,executionsCustom,null);
//	}
//	
//	public void saveBatchRawJob(String folder, String jobInstanceName, boolean hasEnricher,boolean hasTransformer,boolean hasFilter, boolean isCompositeWriter, List<Execution> writers, Map<String,Integer> partitionedParams,Map<String,Integer> parameters,List<Execution> executionsCustom,Object preExecutor) throws IOException {
//		saveBatchRawJob( folder,  jobInstanceName,  hasEnricher, hasTransformer, hasFilter,  isCompositeWriter,writers,partitionedParams,parameters,executionsCustom,true,false,preExecutor);
//	}
//		
//	
//	public void saveBatchRawJob(String folder, String jobInstanceName, boolean hasEnricher,boolean hasTransformer,boolean hasFilter, boolean isCompositeWriter, List<Execution> writers, Map<String,Integer> partitionedParams,Map<String,Integer> parameters,List<Execution> executionsCustom,boolean isSynchronizable,boolean isWriterMassive,Object preExecutor) throws IOException {
//		this.deleteThreeObject(jobInstanceName);
//		
//		String scriptPartitioner = this.getScript(folder+jobInstanceName+"Partitioner.sql");  //required
//		String scriptTestReader = this.getScript(folder+jobInstanceName+"Reader.sql");		//required
//		String scriptTestEnricher = (hasEnricher)?this.getScript(folder+jobInstanceName+"Enricher.js"):"";	//required 
//		String scriptTestTransformer = (hasTransformer)?this.getScript(folder+jobInstanceName+"Transformer.js"):""; //Optional
//		String scriptTestFilter = (hasFilter)?this.getScript(folder+jobInstanceName+"Filter.js"):"";  //Optional
//		String scriptTestWriter = (!isCompositeWriter)?this.getScript(folder+jobInstanceName+"Writer.sql"):"";		//required
//		File f = new File(folder+jobInstanceName+"PreWriter.sql");
//		if( isSynchronizable && ObjectUtils.isEmpty(preExecutor) ) { 
//			 preExecutor = (isSynchronizable)?this.getScript(folder+jobInstanceName+"PreWriter.sql"):"";		//required
//		}
//		
//		Map<String,Object> patientEnricher = this.getExecutor("NoSQL", jobInstanceName+"Enricher", scriptTestEnricher,new HashMap<>());		
//		Execution enricher = (hasEnricher)?this.saveExecution("enricher", "enrichFromNoSQL", patientEnricher, "1"):new Execution();		
//		
//		Map<String,Object> patientTransformer = this.getExecutor("javascript", jobInstanceName+"Transformer", scriptTestTransformer,new HashMap<>());		
//		Execution transformer = (hasTransformer)?this.saveExecution("transformer", "transformerJS", patientTransformer, "2"):new Execution();
//		
//		Map<String,Object> patientFilter = this.getExecutor("javascript", jobInstanceName+"Filter", scriptTestFilter,new HashMap<>());		
//		Execution filter = (hasFilter)?this.saveExecution("filter", "validatorJS", patientFilter, "3"):new Execution();
//		
//		Communication readerComm = this.saveCommunication("dsCarelogicOra");		
//		Communication writerComm = this.saveCommunication("dsCqmVertica");		
//		Communication partitionerComm = this.saveCommunication("dsCarelogicOra");		
//		Communication procesorComm = null;	
//		
//		com.qualifacts.carelogic.batch.model.Process processPartitioner = this.saveProcess("GenericPartitioner","Partitioner",partitionerComm, null, "SQL", jobInstanceName+"Partitioner", scriptPartitioner,GRID_SIZE_CONFIGURED,false,partitionedParams,isWriterMassive,new Execution[0]);
//		com.qualifacts.carelogic.batch.model.Process processReader = this.saveProcess("GenericReaderComponent","ItemReader",readerComm, null, "SQL", jobInstanceName+"Reader", scriptTestReader, false,true,parameters,isWriterMassive,new Execution[0]);			
//	
//		List<Execution> executions = new ArrayList<Execution>();
//		if(null== executionsCustom || executionsCustom.isEmpty()){
//			if(hasEnricher){
//				executions.add(enricher);
//			}
//			if(hasTransformer){
//				executions.add(transformer);
//			}
//			if(hasFilter){
//				executions.add(filter);
//			}
//		}else{
//			executions.addAll(executionsCustom);
//			procesorComm = this.saveCommunication("dsCqmVertica");
//		}
//		com.qualifacts.carelogic.batch.model.Process processProcessor = this.saveProcess("GenericProcessorComponent", "ItemProcessor", procesorComm, null, null, null, null, false, false, parameters,isWriterMassive,executions.toArray(new Execution[executions.size()]));		
//		
//		
//		
//		com.qualifacts.carelogic.batch.model.Process processWriter = this.saveProcess("GenericWriterComponent","ItemWriter",null, writerComm, "sqlWriter", jobInstanceName+"Writer", scriptTestWriter,preExecutor,false,false,parameters,isWriterMassive,writers.toArray(new Execution[writers.size()]));		
//		 
//		Step master = this.saveStep("master", 1, processPartitioner);		
//		Step slave = this.saveStep("slave", 2, processReader, processProcessor , processWriter);		
//		Job jobCqmRaw = this.saveJob(master, slave);
//		
//		ClassLoader classLoader = getClass().getClassLoader();
//		URL url = classLoader.getResource("scripts/"+folder+jobInstanceName+"BeforeExecuteJob.js");
//		if(null != url ){
//			File f1 = new File(url.getFile());			
//			if(f1.exists() && !f1.isDirectory()) { 
//				 String scriptBeforeJob = this.getScript(folder+jobInstanceName+"BeforeExecuteJob.js");
//				 Map<String, Object> executorBeforeJob = this.getExecutor("NoSQL", jobInstanceName+"BeforeExecuteJob", scriptBeforeJob, null);
//				 if(MapUtils.isEmpty(jobCqmRaw.getSettings())){
//					 jobCqmRaw.setSettings(new HashMap());
//				 }
//				 jobCqmRaw.getSettings().put("enrichJobMessage", executorBeforeJob);
//				 jobService.save(jobCqmRaw);
//			}
//		}
////		if(isWriterMassive){
//			jobCqmRaw.setListeners(configureJobListenerMassive(folder,jobInstanceName,parameters,isWriterMassive));
//			jobService.save(jobCqmRaw); 
////		}
//		if(CONFIGURE_SCHEDULE_JOB){
//			this.configureScheduleJob(jobCqmRaw, jobInstanceName);
//		}
//		this.saveJobInstance(jobCqmRaw, jobInstanceName, 1);
//		
//	}
//	
//	public Map<String, Object> configureWriteListener(){
//		Map<String, Object> listenerAudit = new HashMap<>();
//		listenerAudit.put("database", QSI_CQM);
//		listenerAudit.put("collection", "cqmReport");
//		listenerAudit.put("collectionId", "external_id");
//		listenerAudit.put("collectionDetail", "cat1Document");
//		listenerAudit.put("collectionDetailField", "cat1");
//		
//		Map<String, String> auditInQueue = new HashMap<>();
//		auditInQueue.put("exchangeName", "cqm.status.exchange");
//		auditInQueue.put("routingKey", "cqm.status.queue");
//		listenerAudit.put("auditInQueue", auditInQueue);
//		
//		Map<String, String> audit 	= new HashMap<>();
//		audit.put("exchangeName", "cqm.status.exchange");
//		audit.put("routingKey", "cqm.status.queue");
//		listenerAudit.put("audit", audit);
//		
//		Map<String, Object> listener = new HashMap<>();
//		listener.put("AuditItemWriteListener", listenerAudit);
//		
//		
//		return listener;
//	}
//	
//	@SuppressWarnings("unchecked")
//	public Map<String, Object> configureJobListenerMassive(String folder,String jobInstanceName,Map parameters,boolean isMassive) throws IOException{
//		Map<String, Object> listener 	= new HashMap<>();
//		
//		@SuppressWarnings("rawtypes")
//		Map protocolListener 		= new HashMap<>();
//		if(isMassive){
//			Map beforeJobMap = new HashMap<>();
//			beforeJobMap.put("dataSource", "dsCarelogicOra");
//			beforeJobMap.put("scriptCreate", this.getScript(folder+ jobInstanceName+"Create.sql")); 
//			beforeJobMap.put("scriptInsert", this.getScript(folder+ jobInstanceName+"Insert.sql")); 
//			beforeJobMap.put("parameters", parameters);
//			
//			
//			Map afterJobMap  = new HashMap<>();
//			afterJobMap.put("scriptDrop", this.getScript(folder+ jobInstanceName+"Drop.sql")); 
//			afterJobMap.put("dataSource", "dsCarelogicOra");
//			afterJobMap.put("parameters", parameters);
//			
//			
//			ClassLoader classLoader = getClass().getClassLoader();
//			URL url = classLoader.getResource("scripts/"+folder+ jobInstanceName+"AdditionalTask.sql");
//			if(null!=url){
//				File f1 = new File(url.getFile());
//				if(f1.exists() && !f1.isDirectory()){
//					Map additionalTaskMap  = new HashMap<>();
//					additionalTaskMap.put("script", this.getScript(folder+ jobInstanceName+"AdditionalTask.sql")); 
//					additionalTaskMap.put("type", "SQL");
//					additionalTaskMap.put("order", "1");
//					additionalTaskMap.put("dataSource", "dsCqmVertica");
//					additionalTaskMap.put("parameters", parameters);
//					afterJobMap.put("additionalTask", additionalTaskMap);	
//				}				
//			}
//			
//			protocolListener.put("beforeJob", beforeJobMap);
//			protocolListener.put("afterJob", afterJobMap);
//		}
//		
//		Map mailConfigMap  = new HashMap<>();
//		mailConfigMap.put("to", MAIL_TO); 
//		mailConfigMap.put("replyTo",null);
//		mailConfigMap.put("cc","");
//		mailConfigMap.put("bcc",null);
//		mailConfigMap.put("from", "mucqm@qualifacts.com");
//		mailConfigMap.put("subject", "[CQM "+ENV+"] Job "+jobInstanceName+"");
//		mailConfigMap.put("text", "Body");	
//		
//		protocolListener.put("mailConfig", mailConfigMap);	
//
//		listener.put("ProtocolListener", protocolListener);
//		
//		return listener;
//	}
//	
//	public void configureScheduleJob(Job jobCqmRaw,String jobInstanceName){
//		if(MapUtils.isEmpty(jobCqmRaw.getListeners())){
//			jobCqmRaw.setListeners(new HashMap<>());			
//		}
//		if(ObjectUtils.isEmpty(jobCqmRaw.getListeners().get("ProtocolListener"))){
//			jobCqmRaw.getListeners().put("ProtocolListener", new HashMap<>());
//		}
//		
//		List<String> rawJobs = getRawJobsOrderedToExecute();
//		String nextJobInstanceName="";
//		for(int i=0;i<rawJobs.size()-1;i++) {
//			if(rawJobs.get(i).equals(jobInstanceName)){
//				System.out.println(rawJobs.get(i) +" --- "+rawJobs.get(i+1));
//				nextJobInstanceName = rawJobs.get(i+1);
//				break;
//			}			
//		}
//
//		
//		Map<String, Object> scheduleJobConfig 	= new HashMap<>();
//		
//		scheduleJobConfig.put(Field.Batch.JOB_INSTANCE_NAME, nextJobInstanceName);
//		scheduleJobConfig.put(Field.Batch.EXCHANGE_NAME,"batch.exchange");
//		scheduleJobConfig.put(Field.Batch.ROUTING_KEY,"batch.qrda1.queue");
//		((Map<String, Object>)jobCqmRaw.getListeners().get("ProtocolListener")).put("scheduleJobConfig", scheduleJobConfig);
//		jobService.save(jobCqmRaw); 
//	}
//	
//	public Map<String, Object> configureJobListener(){
//		Map<String, Object> listener 	= new HashMap<>();
//				
//		Map<String, String> comm 		= new HashMap<>();
//		comm.put("bodyType", "form-data");
//		comm.put("url", URL_POPHEALTH);
////		comm.put("url", "http://pophealth:3000/api/queries");
//		comm.put("connectionType", "HTTP");
//		Map<String, String> security 	= new HashMap<>();
//		security.put("username", USERNAME);
//		security.put("password", PASSWORD);
//		Map<String, Object> parameters 	= new HashMap<>();
//		parameters.put("measure_id", Types.ARRAY);
//		parameters.put("sub_id", Types.ARRAY);
//		parameters.put("effective_date", Types.VARCHAR);
//		parameters.put("providers[]", Types.ARRAY);
//		
//		Map<String, String> audit 	= new HashMap<>();
//		audit.put("exchangeName", "cqm.status.exchange");
//		audit.put("routingKey", "cqm.status.queue");
//		
//		listener.put("communication", comm);
//		listener.put("security", security);
//		listener.put("parameters", parameters);
//		listener.put("audit", audit);
//		listener.put("auditAsync", false);
//		Map<String, Object> listenerConf = new HashMap<>();
//		listenerConf.put("ProtocolListener", listener);
//		
//		
//		return listenerConf;
//	}
//	
//	public Communication saveCommunicationHTTP(String name){
//		Communication comm = new Communication();
//		comm.setConnectionType("HTTP");
//		comm.setStatus(true);
//		comm.setName(name);
//		Map<String, String> setting = new HashMap<>();
//		setting.put("host", "localhost");
//		setting.put("port", "8080");
//		setting.put("username", "PopHealth");
//		setting.put("password", "PopHealth");
//		setting.put("method", "POST");
//		setting.put("path", "/api/xxxx");
//		comm.setSettings(setting);
//		return communicationService.save(comm);
//	}
//	
//	public Communication saveCommunicationAmqp(String name){
//		Communication comm = new Communication();
//		comm.setConnectionType("AMQP");
//		comm.setStatus(true);
//		comm.setName(name);
//		Map<String, String> setting = new HashMap<>();
//		setting.put("host", "10.2.12.122");//not used
//		setting.put("port", "5672");//not used
//		setting.put("username", "guest");
//		setting.put("password", "guest");
//		setting.put("exchange", "pophealth.exchange");
//		setting.put("exchangeType", "fanout");
//		setting.put("routingKey", "pophealth.qrda1.queue");
//		setting.put("contentType", "application/xml");
//		setting.put("amqp_type", "load-patient");
//
//		comm.setSettings(setting);
//		return communicationService.save(comm);
//	}
//	
//	public void saveQRDAJob(String folder, String jobInstanceName,String measure, boolean hasEnricher,boolean hasTransformer,boolean hasFilter, boolean isCompositeWriter, List<Execution> writers, Map<String,Integer> partitionedParams,Map<String,Integer> parameters) throws IOException {
//		
//		int order=1;
//		//Delete existing jobinstant
//		this.deleteThreeObject(jobInstanceName+"_"+measure);
//		
//		Communication partitionerComm = this.saveCommunication("dsCqmVertica");
//		Communication readerComm = this.saveCommunication("dsCqmVertica");		
//		Communication writerComm = this.saveCommunicationAmqp("AMQPLoadPatient");		
//				
//		
//		String scriptPartitioner = this.getScript(folder+jobInstanceName+"Partitioner.sql");  //required
//		String scriptTestReader = this.getScript(folder+jobInstanceName+"Reader.sql");		//required
//		
//		Map<String,Integer> paramPartitioner = new HashMap<>();	
//		paramPartitioner.put("beginDate", Types.VARCHAR);
//		paramPartitioner.put("endDate", Types.VARCHAR);
//		paramPartitioner.put("providers_practitionerId", Types.ARRAY);
//		paramPartitioner.put("oids", Types.ARRAY);
//		
//		Map<String,Integer> paramReader = new HashMap<>();
//		paramReader.put("fromId",Types.NUMERIC);
//		paramReader.put("toId",Types.NUMERIC);
//		paramReader.putAll(paramPartitioner);
//		
//		com.qualifacts.carelogic.batch.model.Process processPartitioner = this.saveProcess("GenericPartitioner","Partitioner",partitionerComm, null, "SQL", jobInstanceName+"Partitioner", scriptPartitioner,true,false,paramPartitioner,false,new Execution[0]);
//		com.qualifacts.carelogic.batch.model.Process processReader = this.saveProcess("GenericReaderComponent","ItemReader",readerComm, null, "SQL", jobInstanceName+"Reader", scriptTestReader, false,true,paramReader,false,new Execution[0]);			
//	
//
//		String scriptToQRDAHeaderTransformer = (hasTransformer)?this.getScript(folder+jobInstanceName+"Header"+"Transformer.js"):"";  //Optional
//		String scriptValueSetEnricher = (hasEnricher)?this.getScript(folder+"ValueSet"+"Enricher.sql"):"";	//required
//		String scriptAuthorsEnricher = (hasEnricher)?this.getScript(folder+"Authors"+"Enricher.sql"):"";	//required
//		String scriptLegalAuthenticatorEnricher = (hasEnricher)?this.getScript(folder+"LegalAuthenticator"+"Enricher.sql"):"";	//required
//		String scriptProviderEnricher = (hasEnricher)?this.getScript(folder+"Provider"+"Enricher.sql"):"";	//required
//		String scriptOrganizationEnricher = (hasEnricher)?this.getScript(folder+"Organization"+"Enricher.sql"):"";	//required
//		String scriptCustodianEnricher = (hasEnricher)?this.getScript(folder+"Custodian"+"Enricher.sql"):"";	//required
//		String scriptPatientEnricher = (hasEnricher)?this.getScript(folder+"Patient"+"Enricher.sql"):"";	//required
//		String scriptPayerEnricher = (hasEnricher)?this.getScript(folder+"Payer"+"Enricher.sql"):"";	//required
//		String scriptEncounterEnricher = (hasEnricher)?this.getScript(folder+"Encounter"+"Enricher.sql"):"";	//required
//		String scriptDiagnosisEnricher = (hasEnricher)?this.getScript(folder+"Diagnosis"+"Enricher.sql"):"";	//required
//		String scriptProcedureEnricher = (hasEnricher)?this.getScript(folder+"Procedure"+"Enricher.sql"):"";	//required
//		String scriptAssessmentEnricher = (hasEnricher)?this.getScript(folder+"Assessment"+"Enricher.sql"):"";	//required
//		String scriptPhysicalExamEnricher = (hasEnricher)?this.getScript(folder+"PhysicalExam"+"Enricher.sql"):"";	//required
//		String scriptLaboratoryTestEnricher = (hasEnricher)?this.getScript(folder+"LaboratoryTest"+"Enricher.sql"):"";	//required
//		String scriptMedicationEnricher = (hasEnricher)?this.getScript(folder+"Medication"+"Enricher.sql"):"";	//required
//		String scriptMeasuresEnricher = (hasEnricher)?this.getScript(folder+"Measures"+"Enricher.sql"):"";	//required
//		String scriptInterventionEnricher = (hasEnricher)?this.getScript(folder+"Intervention"+"Enricher.sql"):"";	//required
//		String scriptPatientCharEnricher = (hasEnricher)?this.getScript(folder+"PatientChar"+"Enricher.sql"):"";	//required
//		String scriptImmunizationEnricher = (hasEnricher)?this.getScript(folder+"Immunization"+"Enricher.sql"):"";	//required
//		String scriptToQRDAJsonTransformer = (hasTransformer)?this.getScript(folder+jobInstanceName+"Json"+"Transformer.js"):""; //required
//		String scriptCommunicationEnricher = (hasEnricher)?this.getScript(folder+"Communication"+"Enricher.sql"):"";	//required
//
//		
//		List<Execution> executions = new ArrayList<Execution>();
//		//CMS 137		
//		Map<String, Object> valueSetParameter = new HashMap<>();
//		valueSetParameter.put("oids", Types.ARRAY);
//		
//		Map<String, Object> authorsParameter = new HashMap<>();
//		authorsParameter.put("providers_practitionerId", Types.ARRAY);
//		authorsParameter.put("organization", Types.NUMERIC); 
//		
//		Map<String, Object> legalAuthenticatorParameter = new HashMap<>();
//		legalAuthenticatorParameter.put("providers_practitionerId", Types.ARRAY);
//		
//		Map<String, Object> diagnosisParameter = new HashMap<>();
//		diagnosisParameter.put("oids", Types.ARRAY);
//		diagnosisParameter.put("patient_code", Types.NUMERIC); 
//		diagnosisParameter.put("beginDate", Types.VARCHAR);
//		diagnosisParameter.put("endDate", Types.VARCHAR);
//		diagnosisParameter.put("organization", Types.NUMERIC); 
//
//		Map<String, Object> custodianParameter = new HashMap<>();
//		custodianParameter.put("patient_code", Types.NUMERIC); 
//		custodianParameter.put("organization", Types.NUMERIC); 
//		custodianParameter.put("beginDate", Types.VARCHAR);
//		custodianParameter.put("endDate", Types.VARCHAR);
//		
//		Map<String, Object> measuresParameter = new HashMap<>();
//		measuresParameter.put("beginDate", Types.VARCHAR);
//		measuresParameter.put("endDate", Types.VARCHAR);
//		measuresParameter.put("providers_practitionerId", Types.ARRAY);
//		measuresParameter.put("patient_code", Types.NUMERIC); 
//		measuresParameter.put("oids", Types.ARRAY);
//		
//		Map<String, Object> organizationParameter = new HashMap<>();
//		organizationParameter.put("organization", Types.NUMERIC); 
//		
//		Map<String, Object> patientParameter = new HashMap<>();
//		patientParameter.put("patient_code", Types.NUMERIC); 
//		patientParameter.put("organization", Types.NUMERIC); 
//		
//		Map<String, Object> payerParameter = new HashMap<>();
//		payerParameter.put("patient_code", Types.NUMERIC); 
//		payerParameter.put("organization", Types.NUMERIC); 
//		payerParameter.put("oids", Types.ARRAY);
//		
//		Map<String, Object> providerParameter = new HashMap<>();
//		providerParameter.put("providers_practitionerId", Types.ARRAY);
//		
//		Map<String, Object> encounterParameter = new HashMap<>();
//		encounterParameter.put("patient_code", Types.NUMERIC); 
//		encounterParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		encounterParameter.put("endDate", Types.VARCHAR);
//		encounterParameter.put("organization", Types.NUMERIC); 
//		encounterParameter.put("oids", Types.ARRAY);
//		
//		Map<String, Object> procedureParameter = new HashMap<>();
//		procedureParameter.put("patient_code", Types.NUMERIC); 
//		procedureParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		procedureParameter.put("endDate", Types.VARCHAR);		
//		procedureParameter.put("organization", Types.NUMERIC); 
//		procedureParameter.put("oids", Types.ARRAY);
//		
//		Map<String, Object> interventionParameter = new HashMap<>();
//		interventionParameter.put("patient_code", Types.NUMERIC); 
//		interventionParameter.put("organization", Types.NUMERIC); 
//		interventionParameter.put("oids", Types.ARRAY);
//		interventionParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		interventionParameter.put("endDate", Types.VARCHAR);	
//		
//		Map<String, Object> assessmentParameter = new HashMap<>();
//		assessmentParameter.put("patient_code", Types.NUMERIC); 
//		assessmentParameter.put("organization", Types.NUMERIC); 
//		assessmentParameter.put("oids", Types.ARRAY);
//		assessmentParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		assessmentParameter.put("endDate", Types.VARCHAR);	
//		
//		Map<String, Object> physicalExamParameter = new HashMap<>();
//		physicalExamParameter.put("patient_code", Types.NUMERIC); 
//		physicalExamParameter.put("organization", Types.NUMERIC); 
//		physicalExamParameter.put("oids", Types.ARRAY);
//		physicalExamParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		physicalExamParameter.put("endDate", Types.VARCHAR);	
//		
//		
//		Map<String, Object> medicationParameter = new HashMap<>();
//		medicationParameter.put("patient_code", Types.NUMERIC); 
//		medicationParameter.put("organization", Types.NUMERIC); 
//		medicationParameter.put("oids", Types.ARRAY);
//		medicationParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		medicationParameter.put("endDate", Types.VARCHAR);	
//		
//		Map<String, Object> laboratoryTestParameter = new HashMap<>();
//		laboratoryTestParameter.put("patient_code", Types.NUMERIC); 
//		laboratoryTestParameter.put("organization", Types.NUMERIC); 
//		laboratoryTestParameter.put("oids", Types.ARRAY);
//		laboratoryTestParameter.put("beginDate", Types.VARCHAR);
//		laboratoryTestParameter.put("endDate", Types.VARCHAR);	
//		
//		
//		Map<String, Object> patientCharParameter = new HashMap<>();
//		patientCharParameter.put("patient_code", Types.NUMERIC); 
//		patientCharParameter.put("organization", Types.NUMERIC); 
//		patientCharParameter.put("oids", Types.ARRAY);
//		patientCharParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		patientCharParameter.put("endDate", Types.VARCHAR);
//		
//		Map<String, Object> inmunizationParameter = new HashMap<>();
//		inmunizationParameter.put("patient_code", Types.NUMERIC); 
//		inmunizationParameter.put("organization", Types.NUMERIC); 
//		inmunizationParameter.put("oids", Types.ARRAY);
//		inmunizationParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		inmunizationParameter.put("endDate", Types.VARCHAR);
//		
//		Map<String, Object> communicationParameter = new HashMap<>();
//		communicationParameter.put("patient_code", Types.NUMERIC); 
//		communicationParameter.put("organization", Types.NUMERIC); 
//		communicationParameter.put("oids", Types.ARRAY);
//		communicationParameter.put("actualPeriodStartDate", Types.VARCHAR);
//		communicationParameter.put("endDate", Types.VARCHAR);		
//		
//		executions.add(this.saveExecution("transformer", "transformerJS", this.getExecutor("javascript", jobInstanceName+"Header"+"Transformer", scriptToQRDAHeaderTransformer,new HashMap<>()),String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "ValueSetEnricher", scriptValueSetEnricher,valueSetParameter,"MapKeyValue",true), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Authors"+"Enricher", scriptAuthorsEnricher,authorsParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "LegalAuthenticator"+"Enricher", scriptLegalAuthenticatorEnricher,legalAuthenticatorParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Provider"+"Enricher", scriptProviderEnricher,providerParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Organization"+"Enricher", scriptOrganizationEnricher,organizationParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Custodian"+"Enricher", scriptCustodianEnricher,custodianParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Patient"+"Enricher", scriptPatientEnricher,patientParameter), String.valueOf(order++)));
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Payer"+"Enricher", scriptPayerEnricher,payerParameter), String.valueOf(order++)));				
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Encounter"+"Enricher", scriptEncounterEnricher,encounterParameter), String.valueOf(order++)));
//		
//		switch (jobInstanceName+"_"+measure) {
//		case "QRDA1_CMS137v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//		}break;	
//		case "QRDA1_CMS68v6":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//		}break;	
//		case "QRDA1_CMS177v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//		}break;	
//		case "QRDA1_CMS161v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//		}break;	
//		case "QRDA1_CMS160v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Assessment"+"Enricher", scriptAssessmentEnricher,assessmentParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PatientChar"+"Enricher", scriptPatientCharEnricher,patientCharParameter), String.valueOf(order++)));
//
//		}break;	
//		
//		//--------Spring 4
//		case "QRDA1_CMS154v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//
//			
//		}break;	
//		case "QRDA1_CMS169v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//			
//		}break;	
//		case "QRDA1_CMS165v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PhysicalExam"+"Enricher", scriptPhysicalExamEnricher,physicalExamParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			
//		}break;
//		case "QRDA1_CMS138v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PatientChar"+"Enricher", scriptPatientCharEnricher,patientCharParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Assessment"+"Enricher", scriptAssessmentEnricher,assessmentParameter), String.valueOf(order++)));
//		}break;
//		case "QRDA1_CMS69v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PhysicalExam"+"Enricher", scriptPhysicalExamEnricher,physicalExamParameter), String.valueOf(order++)));
//			
//		}break;
//		case "QRDA1_CMS2v6":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Assessment"+"Enricher", scriptAssessmentEnricher,assessmentParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//		}break;
//		//---------------------------- New Measures ----------------------------
//		case "QRDA1_CMS122v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "LaboratoryTest"+"Enricher", scriptLaboratoryTestEnricher,laboratoryTestParameter), String.valueOf(order++)));
//		}break;
//		case "QRDA1_CMS136v6":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//		}break;
//		case "QRDA1_CMS139v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Assessment"+"Enricher", scriptAssessmentEnricher,assessmentParameter), String.valueOf(order++)));
//		}break;
//		case "QRDA1_CMS50v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Communication"+"Enricher", scriptCommunicationEnricher,communicationParameter), String.valueOf(order++)));
//			
//		}break;
//		case "QRDA1_CMS128v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Medication"+"Enricher", scriptMedicationEnricher,medicationParameter), String.valueOf(order++)));
//		}break;
//		case "QRDA1_CMS65v6":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PhysicalExam"+"Enricher", scriptPhysicalExamEnricher,physicalExamParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//
//		}break;
//		case "QRDA1_CMS159v5":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Intervention"+"Enricher", scriptInterventionEnricher,interventionParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Assessment"+"Enricher", scriptAssessmentEnricher,assessmentParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "PatientChar"+"Enricher", scriptPatientCharEnricher,patientCharParameter), String.valueOf(order++)));
//			
//		}break;
//		case "QRDA1_CMS147v6":{
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Diagnosis"+"Enricher", scriptDiagnosisEnricher,diagnosisParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Procedure"+"Enricher", scriptProcedureEnricher,procedureParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Immunization"+"Enricher", scriptImmunizationEnricher,inmunizationParameter), String.valueOf(order++)));
//			executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Communication"+"Enricher", scriptCommunicationEnricher,communicationParameter), String.valueOf(order++)));
//		}break;
//		
//		default:
//			break;
//		}
//		executions.add(this.saveExecution("enricher", "enrichFromSQL", this.getExecutor("SQL", "Measures"+"Enricher", scriptMeasuresEnricher,measuresParameter), String.valueOf(order++)));
//		
//		executions.add(this.saveExecution("transformer", "transformerJS", this.getExecutor("javascript", jobInstanceName+"Json"+"Transformer", scriptToQRDAJsonTransformer,new HashMap<>()), String.valueOf(order++)));
//		executions.add(this.saveExecution("transformer", "transformerXML", this.getExecutor("velocity", jobInstanceName+"Xml"+"Transformer", getTemplatesCat1(),new HashMap<>()), String.valueOf(order++)));
//				
//		com.qualifacts.carelogic.batch.model.Process processProcessor = this.saveProcess("GenericProcessorComponent", "ItemProcessor", readerComm, null, null, null, null, false, false, parameters,false,executions.toArray(new Execution[executions.size()]));		
//
//		
//		Map<String, Integer> writerParameter = new HashMap<>();
//		writerParameter.put("practice_name", Types.VARCHAR);
//		writerParameter.put("patientId", Types.VARCHAR);
//		writerParameter.put("firstName", Types.VARCHAR);
//		writerParameter.put("lastName", Types.VARCHAR);
//		writerParameter.put("external_id", Types.VARCHAR);
//		
//		com.qualifacts.carelogic.batch.model.Process processWriter = this.saveProcess("GenericWriterComponent","ItemWriter",null, writerComm, "amqpWriter", "loadPatient", "xml",false,false,writerParameter,false,writers.toArray(new Execution[writers.size()]));		
//		processWriter.setListener(configureWriteListener());
//		processService.save(processWriter);
//		
//		Step master = this.saveStep("master", 1, processPartitioner);		
//		Step slave = this.saveStep("slave", 2, processReader, processProcessor , processWriter);		
//		
//		Job jobCqmRaw = this.saveJob(master, slave);	
//		jobCqmRaw.setListeners(configureJobListener());
//		jobService.save(jobCqmRaw); 
//		
//		this.saveJobInstance(jobCqmRaw, jobInstanceName+"_"+measure, 1);
//		
//	}
//

//	public String getEntriesEnricher(List<String> enrichers){
//		StringBuffer stringBuffer=new StringBuffer();
//		for (String string : enrichers) {
//			stringBuffer.append(string+" union all \n");
//			
//		}
//		return stringBuffer.substring(0,stringBuffer.lastIndexOf(" union all"));
//	}
//	

	
	public String getFileToStrin(String path) throws Exception{
		File f = new File(path);
		if(f.exists() && !f.isDirectory()) { 
			return new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
		}
		return "<!--XXXXXXXXXXXXXXXX Template doesnt Loaded XXXXXXXXXXXXXXXX-->";
	}
	
	//@Test
	public void testRootPath() throws IOException{
		String rootProjectPath = new File(new File(".").getCanonicalPath()).getParent();
		String pathRoot = rootProjectPath+ "/carelogic-batch-api/src/main/resources/config/integration/cqm/templates/cat1/";
		Assert.notNull(pathRoot);
	}
}
