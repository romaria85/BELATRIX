package org.belatrix.constant;


public final class Field {

	private Field() {
	}
	
	
	public final class Partitioner {
		private Partitioner() {
		}

		public static final String SCRIPT = "script";
		public static final String GRID_SIZE = "gridSize";
		public static final String FETCH_SIZE = "fetchSize";
		public static final String TO_ID = "toId";
		public static final String FROM_ID = "fromId";
		public static final String NUMBER_BLOCK = "numberBlock";
		public static final String ROW_MIN = "MIN";
		public static final String ROW_MAX = "MAX";
		public static final String PARTITION = "worker";
		public static final String PARAMETERS = "parameters";
		public static final String EXCHANGE_NAME = "exchangeName";
		public static final String ROUTING_KEY = "routingKey";
		public static final String FROM_DATE = "fromDate";		
		public static final String TO_DATE = "toDate";
		public static final String DATE_REFERENCED = "dateReference";
		
	}
	
	
}
