package org.belatrix.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Task extends AbstractDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8153163162285240656L;
	
	private String url;
	private String status;
	private String outputSearched;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOutputSearched() {
		return outputSearched;
	}
	public void setOutputSearched(String outputSearched) {
		this.outputSearched = outputSearched;
	}
	
	
}
