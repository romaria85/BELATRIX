package org.belatrix;

import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

public class ApplicationProperties {
	
	
	@Value("exchange.queue")
	protected String exchangeCqmPopHealth = "exchange.queue";
	
	@Value("queue")
	protected String queueLoadPatientCqm = "queue";
	
	@Value("3")
	protected int concurrentConsumers = 3;
	
	@Value("10")
	protected int maxConcurrentConsumers = 10;	
	
	/**
	 * RabbitMQ port.
	 */
	@Value("5672")
	protected int port = 5672;

	/**
	 * Login user to authenticate to the broker.
	 */
	@Value("guest")
	protected String username = "guest";

	/**
	 * Login to authenticate against the broker.
	 */
	@Value("guest")
	protected String password = "guest";

	/**
	 * Virtual host to use when connecting to the broker.
	 */
	@Value("/")
	protected String virtualHost ="/";

	/**
	 * Comma-separated list of addresses to which the client should connect to.
	 */
	@Value("localhost")
	protected String addresses;

	/**
	 * Requested heartbeat timeout, in seconds; zero for none.
	 */
	//@Value("${server.rabbitmq.requested-heartbeat}")
	@Value("0")
	protected Integer requestedHeartbeat;

	public int getPort() {
		if (this.addresses == null) {
			return this.port;
		}
		String[] hosts = StringUtils.delimitedListToStringArray(this.addresses, ":");
		if (hosts.length >= 2) {
			return Integer.valueOf(StringUtils.commaDelimitedListToStringArray(hosts[1])[0]);
		}
		return this.port;
	}

	public void setAddresses(String addresses) {
		this.addresses = parseAddresses(addresses);
	}

	public String getAddresses() {
		return (this.addresses == null ? "localhost" + ":" + this.port : this.addresses);
	}

	private String parseAddresses(String addresses) {
		Set<String> result = new LinkedHashSet<String>();
		for (String address : StringUtils.commaDelimitedListToStringArray(addresses)) {
			address = address.trim();
			if (address.startsWith("amqp://")) {
				address = address.substring("amqp://".length());
			}
			if (address.contains("@")) {
				String[] split = StringUtils.split(address, "@");
				String creds = split[0];
				address = split[1];
				split = StringUtils.split(creds, ":");
				this.username = split[0];
				if (split.length > 0) {
					this.password = split[1];
				}
			}
			int index = address.indexOf("/");
			if (index >= 0 && index < address.length()) {
				setVirtualHost(address.substring(index + 1));
				address = address.substring(0, index);
			}
			if (!address.contains(":")) {
				address = address + ":" + this.port;
			}
			result.add(address);
		}
		return (result.isEmpty() ? null : StringUtils.collectionToCommaDelimitedString(result));
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getVirtualHost() {
		return this.virtualHost;
	}

	public void setVirtualHost(String virtualHost) {
		this.virtualHost = ("".equals(virtualHost) ? "/" : virtualHost);
	}

	public Integer getRequestedHeartbeat() {
		return this.requestedHeartbeat;
	}

	public void setRequestedHeartbeat(Integer requestedHeartbeat) {
		this.requestedHeartbeat = requestedHeartbeat;
	}
}
