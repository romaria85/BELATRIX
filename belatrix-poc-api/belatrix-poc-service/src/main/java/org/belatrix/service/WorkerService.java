package org.belatrix.service;

import java.util.List;

import org.belatrix.model.Worker;

/**
 * 
 * @author jalor
 *
 */
public interface WorkerService extends GenericService<Worker, String> {
	
	List<Worker> findByName(String name);

}
