package org.belatrix.service.impl;

import org.belatrix.model.Execution;
import org.belatrix.repository.ExecutionRepository;
import org.belatrix.service.ExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jalor
 *
 */
@Service
public class ExecutionServiceImpl implements ExecutionService {
	
	@Autowired
	private ExecutionRepository repo;

	@Override
	public Execution findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public Execution save(Execution entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(Execution entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		
		return repo.exists(id);
	}


	
}
