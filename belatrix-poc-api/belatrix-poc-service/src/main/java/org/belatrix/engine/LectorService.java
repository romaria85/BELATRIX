package org.belatrix.engine;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

/**
 * 
 * @author j@lor
 *
 */

@Service
public class LectorService {
	
	public  List<String> urls ; 
	public  String chartSetDefault = "UTF-8"; 
	
	
	public  Boolean loadFileWithUrls(String route){
		
		Boolean loadFile = true;
		
		Path path = Paths.get(route);
		Charset charset = Charset.forName(chartSetDefault);
		
		urls = Collections.synchronizedList(new ArrayList<String>());
		
		try (Stream<String> stream = Files.lines(path,charset)) {
			   stream.forEach( (v)->{
				   try {
					urls.add(v);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   });
			} catch (IOException e) {
			   e.printStackTrace();
			   loadFile = false;
		}
		
		return loadFile;
	}

	public List<String> getUrls() {
		return urls;
	}

	public void clearUrls() {
		urls.clear(); 
	}
}
