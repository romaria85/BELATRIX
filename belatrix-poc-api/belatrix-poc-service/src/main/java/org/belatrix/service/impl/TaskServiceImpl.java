package org.belatrix.service.impl;

import org.belatrix.model.Task;
import org.belatrix.repository.TaskRepository;
import org.belatrix.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jalor
 *
 */
@Service
public class TaskServiceImpl implements TaskService {
	
	@Autowired
	private TaskRepository repo;

	@Override
	public Task findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public Task save(Task entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(Task entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		
		return repo.exists(id);
	}

}
