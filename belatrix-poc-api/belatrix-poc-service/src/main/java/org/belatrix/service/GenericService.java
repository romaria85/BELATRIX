package org.belatrix.service;

import java.io.Serializable;

/**
 * 
 * @author j@lor
 *
 * @param <T>
 * @param <ID>
 */
public interface GenericService<T, ID extends Serializable> {

	T findById(ID id);
	
	T save(T entity);
	
	void delete(T entity);
	
	boolean exists(ID id);

}
