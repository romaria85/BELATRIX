package org.belatrix.component;

import org.belatrix.canonical.QSIGenericMessage;
import org.belatrix.model.Execution;
import org.springframework.stereotype.Component;


/**
 * Implementation of validation using Script's Groovy, JS, Scala, and others.
 * 
 * @author jalor
 *
 */
@Component("validatorJS")
public class ScriptValidator implements ValidatorStrategy {

	@Override
	public boolean validate(Execution execution, QSIGenericMessage message) {
		 return true;
	}

}
