package org.belatrix.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.belatrix.constant.Field;
import org.belatrix.model.Process;
import org.belatrix.model.ProcessLogger;
import org.belatrix.model.Worker;
import org.belatrix.service.ProcessLoggerService;
import org.belatrix.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author jalor
 *
 */
@Service
@Scope("prototype")
public class BusinessLogicImpl implements BusinessLogic {
	
	@Autowired
	protected ProcessLoggerService processLoggerServiceImpl;
	
	@Autowired
	protected WorkerService workerServiceImpl;
	
	public ProcessLogger processSaveExecution(Process process , Map<String, Worker> workersMap ,String patternCode,List<String> urls) {
		ProcessLogger currentProcessLogger = new ProcessLogger();
		currentProcessLogger.setStatus("REGISTER");
		currentProcessLogger.setProcess(process);
		currentProcessLogger.setUrls(urls);
		
		List<Worker> workers = new ArrayList<Worker>();
		currentProcessLogger.setWorkers(workers);
		currentProcessLogger.setPatternCode(patternCode);
		Worker worker = null;
		
		int i= 1;
		for (Map.Entry<String, Worker> entry : workersMap.entrySet()) {
			
			worker = entry.getValue();
			worker.setName(Field.Partitioner.PARTITION +1);
			worker.setStatus("REGISTER");
			worker.setOrder(String.valueOf(i));
			i++;
			workers.add(worker);
		}
		
		
		saveProcessLoggerWithWorkers(currentProcessLogger);
		
		/**
		 *  Send process to queue!
		 */
		
		
		return currentProcessLogger;
	}
	
	@Transactional(value="REQUIRES_NEW")
	protected ProcessLogger saveProcessLoggerWithWorkers(ProcessLogger entity) {
		
		
		entity.getWorkers().forEach((final Worker worker) -> {
			System.out.println(worker.toString());
			workerServiceImpl.save(worker);
		});
		
		processLoggerServiceImpl.save(entity);
		return entity;
		
	}
}
