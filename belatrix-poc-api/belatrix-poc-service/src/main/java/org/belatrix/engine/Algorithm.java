package org.belatrix.engine;

import java.util.Map;
import org.belatrix.model.Process;
import org.belatrix.model.Worker;



/**
 * Algorithm Partitioning Grid or Partition
 * 
 * @author j@lor
 */
public interface Algorithm {
	
	Map<String, Worker> process(Map<String, Object> resultCount,
			Process currentProcess,
			int req);

}
