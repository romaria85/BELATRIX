package org.belatrix.service.impl;

import java.util.List;

import org.belatrix.model.Worker;
import org.belatrix.repository.WorkerRepository;
import org.belatrix.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jalor
 *
 */
@Service
public class WorkerServiceImpl implements WorkerService {
	
	@Autowired
	private WorkerRepository repo;

	@Override
	public Worker findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public Worker save(Worker entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(Worker entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		
		return repo.exists(id);
	}

	@Override
	public List<Worker> findByName(String name) {
		return repo.findByName(name);
	}
	
	
}
