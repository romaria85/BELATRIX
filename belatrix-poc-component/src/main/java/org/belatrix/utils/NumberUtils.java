package org.belatrix.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

	
	public static double roundUP(double number, int zeroes) {
		BigDecimal value = new BigDecimal(number);
		value = value.setScale(zeroes, RoundingMode.UP);
		return value.doubleValue();
	}

}
