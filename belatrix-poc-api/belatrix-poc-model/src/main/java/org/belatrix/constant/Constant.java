package org.belatrix.constant;

/**
 * 
 * @author jalor
 *
 */
public class Constant {
	
	public static final String NAME 				= "name";
	public static final String SCRIPT_TYPE 			= "scriptType";
	public static final String SQL 					= "sql";
	public static final String JS 					= "js";
	public static final String SETTING 				= "setting";
	public static final String URL 					= "url";
	public static final String USER 				= "user";
	public static final String SCRIPT 			    = "script";
	public static final String EXECUTIONS 			= "executions";
	public static final String PROCESSES 			= "process";
	public static final String _ID 					= "id";
	
}
