package org.belatrix.component.listener;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import org.belatrix.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

@Component
public class AbstractQueueListener extends ApplicationProperties {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractQueueListener.class);

	protected Map<String, Object> mapMessage;
	
	public void onMessage(Object message, Map<String, Object> headers) {
		LOGGER.info(" headers : {}", headers);
		LOGGER.info(" message : {}", message);
		try {
			setHeadersToContext(headers);
			Message msg = (Message) message;
			
			ByteArrayInputStream byteIn = new ByteArrayInputStream(msg.getBody());
		    ObjectInputStream in = new ObjectInputStream(byteIn);
		    
			Map<String, Object> data = (Map<String, Object>) in.readObject();
			mapMessage = data;
			
		} catch (Exception e) {
			mapMessage = new HashMap<>();
			LOGGER.error(e.getMessage(), e);
		}
	}

	public void setHeadersToContext(Map<String, Object> header) {
		Map<String, String> newHeader = new HashMap<>();
	}
}
