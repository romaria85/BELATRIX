package org.belatrix.component;

import org.belatrix.canonical.QSIGenericMessage;
import org.belatrix.model.Execution;

/**
 * It defines a common interface to the algorithms of search supports.
 * 
 * @author jalor
 *
 */
public interface EnricherStrategy {
	
	QSIGenericMessage enrich(Execution execution, QSIGenericMessage item);

}
