package org.belatrix.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Pattern extends AbstractDocument {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1872154523273888749L;
	private String name;
	private String code;
	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
