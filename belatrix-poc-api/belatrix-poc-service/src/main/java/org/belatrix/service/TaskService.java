package org.belatrix.service;

import org.belatrix.model.Task;

/**
 * 
 * @author jalor
 *
 */
public interface TaskService extends GenericService<Task, String> {

}
