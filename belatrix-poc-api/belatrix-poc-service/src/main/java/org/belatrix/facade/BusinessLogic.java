package org.belatrix.facade;

import java.util.List;
import java.util.Map;

import org.belatrix.model.Process;
import org.belatrix.model.ProcessLogger;
import org.belatrix.model.Worker;

/**
 * 
 * @author jalor
 *
 */
public interface BusinessLogic {
	

	public ProcessLogger processSaveExecution(Process process , Map<String, Worker> workersMap,String patternCode,List<String> urls) ;
}
