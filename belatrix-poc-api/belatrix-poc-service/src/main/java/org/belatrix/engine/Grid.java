package org.belatrix.engine;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.belatrix.constant.Field;
import org.belatrix.model.Worker;
import org.belatrix.model.Process;
import org.belatrix.util.NumberUtil;
import org.springframework.stereotype.Component;



/**
 * Implementation of generic partitioner for creating input parameters for a partitioned worker. 
 * 
 * @author jalor
 *
 */
@Component("gridAlgorithm")
public class Grid implements Algorithm{

	private static final Log LOG = LogFactory.getLog(Grid.class);

	/**
	 * This method was changed in order to proceed based on grid size strategy example (rows / grid size) = partitions
	 */
	@Override
	public Map<String, Worker> process(Map<String, Object> resultCount,
						Process currentProcess,int reqGridSize) {
		String setGridSize = currentProcess.getGridSize();
		
		int partitions = 0;
		int min = NumberUtil.toInteger(resultCount.get(Field.Partitioner.ROW_MIN));
		int max = NumberUtil.toInteger(resultCount.get(Field.Partitioner.ROW_MAX));
		
		int gridSize = (setGridSize != null ? Integer.parseInt(setGridSize) : reqGridSize);
		int length = max - min + 1;
		int fromId = min;
		int toId   = min + gridSize - 1;
		
		partitions = (length<=gridSize)? 1:(int)NumberUtil.roundUP(((double)(length)/gridSize),0);
		
		Map<String, Worker> workers = new HashMap<String, Worker>();
		Map<String, Object> parameters = null;
		
		for(int i=1;i<=partitions;i++){
			if (toId >= max) {
				toId = max;
			}
			int fetchSize = (toId - fromId) + 1;
			Worker value = new Worker();
			parameters = new HashMap<>();
			workers.put(Field.Partitioner.PARTITION + i, value);
			value.setParameters(parameters);
			parameters.put(Field.Partitioner.NUMBER_BLOCK, i);
			parameters.put(Field.Partitioner.FETCH_SIZE, fetchSize);
			parameters.put(Field.Partitioner.FROM_ID, fromId);
			parameters.put(Field.Partitioner.TO_ID, toId);
			parameters.put(Field.Partitioner.GRID_SIZE, max);
			
			LOG.info("from: "+fromId+" toId: "+toId+" numberBlock: "+i+" fetchSize:"+fetchSize);
			fromId = toId + 1;
			toId += gridSize;
		}
		LOG.debug(workers.toString());
        return workers;
    }

}
