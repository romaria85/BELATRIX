package org.belatrix.engine;

/**
 * Partitioner  for creating input parameters for a partitioned worker
 * 
 * @author j@lor
 */
public interface Partitioner {
	
   void processPartition(String route , String patternCode);

}
