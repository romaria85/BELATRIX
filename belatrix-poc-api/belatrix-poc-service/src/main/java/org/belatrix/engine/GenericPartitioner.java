package org.belatrix.engine;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.belatrix.constant.Constant;
import org.belatrix.constant.Field;
import org.belatrix.engine.exception.BusinessException;
import org.belatrix.facade.BusinessLogicImpl;
import org.belatrix.model.Process;
import org.belatrix.model.ProcessLogger;
import org.belatrix.model.Worker;
import org.belatrix.service.ProcessService;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;


/**
 * Implementation of generic partitioner for creating input parameters for a partitioned worker. 
 * 
 * @author jalor
 *
 */
@Component
@Scope("prototype")
public class GenericPartitioner implements Partitioner {

	private static final Log LOG = LogFactory.getLog(GenericPartitioner.class);

	public static final String ALGORITHM_NAME = "gridAlgorithm";
	public static final String ROW_MIN = "1";
	public static final int reqGridSizeDefault = 5;
	
	@Autowired
	private BeanFactory beanFactory;
	
	@Autowired
	LectorService lectorService;
	
	@Autowired
	BusinessLogicImpl businessLogic;
	
	
	@Autowired
    ProcessService processServiceImpl;
	
	@Autowired
	org.springframework.data.mongodb.core.MongoTemplate mongoTemplate;
	/**
	 * This method was changed in order to proceed based on grid size strategy example (rows / grid size) = partitions
	 */
	
	@Autowired
	protected RabbitMessagingTemplate rabbitMessagingTemplate;
	
	public void processPartition(String route , String patternCode) {
		
		Map<String, Object> messageMap = null;
		try {
			
			List<Process> processList = processServiceImpl.findByName("Parseador");
			Assert.isTrue( processList.size() == 1 , "The value must be equal to 1");
			
			Process currentProcess =  processList.get(0);
			String nameAlgorithm =    currentProcess.getNameAlgorithm();
			
			/**
			 *  Called to load urls!
			 */
			LOG.debug("Called to load urls!");
			
			lectorService.loadFileWithUrls(route);
			
			if (lectorService.getUrls().size() > 0 ) {
				
				Map<String, Object> resultCount = new HashMap<String, Object>();
				resultCount.put(Field.Partitioner.ROW_MIN, ROW_MIN);
				resultCount.put(Field.Partitioner.ROW_MAX, String.valueOf(lectorService.getUrls().size()));
				 Map<String, Worker> workers = null;
				if(ALGORITHM_NAME.equalsIgnoreCase(nameAlgorithm)){
					workers= ((Algorithm)beanFactory.getBean(ALGORITHM_NAME)).process(resultCount, currentProcess, reqGridSizeDefault);
					
					LOG.info("preparing object for send to queue!!");
					ProcessLogger processLogger = businessLogic.processSaveExecution(currentProcess, workers,patternCode,lectorService.getUrls());
					
					String _idProcessLogger = processLogger.getId();
					messageMap = new HashMap<>();
					messageMap.put(Constant._ID, _idProcessLogger);
					
					byte[] obis = convertMessageMapToByte(messageMap);
					Message<Object> message = new GenericMessage<>(obis, new HashMap<>());
					rabbitMessagingTemplate.send( "exchange.queue","rkey.queue" , message);
				}
				
			}
		}catch (Exception e) {
			LOG.error(e.getMessage(),e);
			throw new BusinessException(e.getMessage());
		}finally {
			if (!ObjectUtils.isEmpty(messageMap)) {
				LOG.info("Send new message who id :" + messageMap.get(Constant._ID) );
			}
        }
		
	}
	
	private byte[] convertMessageMapToByte(Map<String, Object> messageMap) throws IOException {
		
		ByteArrayOutputStream bos  = new ByteArrayOutputStream();
		ObjectOutputStream oos  = new ObjectOutputStream(bos);
		oos .writeObject(messageMap);
		oos .flush();
		byte[] obis = bos.toByteArray();
		oos.close();
        bos.close();
        
        return obis;
	}
	
}
