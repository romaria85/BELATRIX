package org.belatrix.util;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

import org.belatrix.engine.LectorService;
import org.junit.Test;

/**
 * 
 * @author j@lor
 *
 */
public class LectorUtilTest {
	
	public  List<String> urls = new ArrayList<String>();  
	 
	
	@Test
	public  void readerFileTest1(){
		
		Path path = Paths.get("F://git//urls.txt");
		Charset charset = Charset.forName("UTF-8");
		
		try (Stream<String> stream = Files.lines(path)) {
			   stream.forEach( (v)->{
				   try {
					urls.add(v);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   });
			} catch (IOException e) {
			   e.printStackTrace();
		}
		
		
	}

	public List<String> getUrls() {
		return urls;
	}

	
	
}
