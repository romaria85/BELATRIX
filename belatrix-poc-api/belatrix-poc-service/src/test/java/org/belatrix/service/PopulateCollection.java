package org.belatrix.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.belatrix.constant.Field;
import org.belatrix.engine.Algorithm;
import org.belatrix.model.Pattern;
import org.belatrix.model.Process;
import org.belatrix.model.Worker;
import org.belatrix.util.LectorUtilTest;
import org.belatrix.util.StringUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * 
 * @author jalor
 *
 */
public class PopulateCollection extends AbstractServiceTest {

	@Autowired
	protected ProcessService processServiceImpl;
	
	@Autowired
	PatternService patternServiceImpl;
	
	@Autowired
	@Qualifier("gridAlgorithm")
	Algorithm	algorithm;
	
	@Test
	public void loadProcessWithPatterns() {
		
		List<Process> list = processServiceImpl.findByName("Parseador");
		Process process =new Process();
		process.setName("Parseador");
		process.setGridSize("5");
		process.setNameAlgorithm("gridAlgorithm");
		
		List<Pattern> patternList = new ArrayList<Pattern>();
		Pattern pattern1 = new Pattern();
		pattern1.setCode("code1");
		pattern1.setName("#hashTag");
		patternList.add(pattern1);
		patternServiceImpl.save(pattern1);
		
		Pattern pattern2 = new Pattern();
		pattern2.setCode("code2");
		pattern2.setName("@account");
		patternList.add(pattern2);
		patternServiceImpl.save(pattern2);

		process.setPatterns(patternList);
		processServiceImpl.save(process);
		
	}
	
	@Test
	public void partitionedListOfFile() {
		LectorUtilTest lectorUtil = new LectorUtilTest();
		lectorUtil.readerFileTest1();
		
		Map<String, Object> resultCount = new HashMap<String, Object>();
		
		resultCount.put(Field.Partitioner.ROW_MIN, "1");
		resultCount.put(Field.Partitioner.ROW_MAX, String.valueOf(lectorUtil.getUrls().size()));
		
		Map<String, Worker> workers = null;
		workers = algorithm.process(resultCount, null, 5);
		System.out.println(workers.toString());
	}
	
}
