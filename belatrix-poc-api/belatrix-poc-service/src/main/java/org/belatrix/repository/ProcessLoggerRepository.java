package org.belatrix.repository;

import java.util.List;
import org.belatrix.model.ProcessLogger;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author jalor
 *
 */
@Repository
public interface ProcessLoggerRepository extends MongoRepository<ProcessLogger, String> {
	
	List<ProcessLogger> findByName(String name);

}
