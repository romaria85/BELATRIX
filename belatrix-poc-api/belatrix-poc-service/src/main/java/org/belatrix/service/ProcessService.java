package org.belatrix.service;

import java.util.List;

import org.belatrix.model.Process;

/**
 * 
 * @author jalor
 *
 */
public interface ProcessService extends GenericService<Process, String> {
	
	List<Process> findByName(String name);
	 List<Process> findAll() ;

}
