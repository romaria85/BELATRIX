package org.belatrix.service.impl;

import org.belatrix.repository.ProcessRepository;
import org.belatrix.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import org.belatrix.model.Process;

/**
 * 
 * @author jalor
 *
 */
@Service
public class ProcessServiceImpl implements ProcessService {
	
	@Autowired
	private ProcessRepository repo;

	@Override
	public Process findById(String id) {
		return repo.findOne(id);
	}

	@Override
	public Process save(Process entity) {
		return repo.save(entity);
	}

	@Override
	public void delete(Process entity) {
		repo.delete(entity);
	}

	@Override
	public boolean exists(String id) {
		
		return repo.exists(id);
	}

	@Override
	public List<Process> findByName(String name) {
		return repo.findByName(name);
	}
	
	@Override
	public List<Process> findAll() {
		return repo.findAll();
	}
	
}
