package org.belatrix.queue.config;

import java.util.HashMap;
import java.util.Map;

import org.belatrix.ApplicationProperties;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;



@Configuration
public class ApplicationAmqpConfig extends ApplicationProperties {

	@Bean
	public ConnectionFactory defaultConnectionFactory() {

		CachingConnectionFactory cf = new CachingConnectionFactory();
		cf.setAddresses(this.getAddresses());
		cf.setUsername(this.getUsername());
		cf.setPassword(this.getPassword());
		cf.setVirtualHost(this.getVirtualHost());
		cf.setRequestedHeartBeat(this.getRequestedHeartbeat());
		return cf;
	}

	@Bean
	public SimpleRabbitListenerContainerFactory listenerContainerFactory() {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(defaultConnectionFactory());
		factory.setConcurrentConsumers(3);
		factory.setMaxConcurrentConsumers(10);
		factory.setAdviceChain(RetryInterceptorBuilder.stateless().maxAttempts(3).backOffOptions(3000, 1.0, 3000).build());
		factory.setDefaultRequeueRejected(false);
		return factory;
	}

	@Bean
	public AmqpTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(defaultConnectionFactory());
		RetryTemplate retryTemplate = new RetryTemplate();
		ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
		backOffPolicy.setInitialInterval(500);
		backOffPolicy.setMultiplier(10.0);
		backOffPolicy.setMaxInterval(10000);
		retryTemplate.setBackOffPolicy(backOffPolicy);
		template.setRetryTemplate(retryTemplate);
		return template;
	}

	@Bean
	public RabbitMessagingTemplate rabbitMessagingTemplate() {
		return new RabbitMessagingTemplate((RabbitTemplate) rabbitTemplate());
	}
	
	@Bean
	public Exchange cqmPopHealthExchange() {
		return new FanoutExchange(this.exchangeCqmPopHealth);
	}

	@Bean
	public Exchange errorExchange() {
		return new DirectExchange("exchange.error");
	}

		
	@Bean
	public Queue queueLoadPatientCqm() {
		return new Queue(this.queueLoadPatientCqm, true, false, false, getFailureArguments());
	}
	

	
	@Bean
	public Queue failureQueue() {
		return new Queue("error");
	}

	
	@Bean
	@Autowired
	public Binding failureQueueBinding(@Qualifier("failureQueue") Queue failureQueue, @Qualifier("errorExchange") Exchange errorExchange) {
		return BindingBuilder.bind(failureQueue).to(errorExchange).with("rkey.error").noargs();
	}

	
	@Bean
	@Autowired
	public Binding cqmLoadPatientBinding(@Qualifier("queueLoadPatientCqm")Queue queueLoadPatientCqm, @Qualifier("cqmPopHealthExchange") Exchange cqmPopHealthExchange) {
		return BindingBuilder.bind(queueLoadPatientCqm).to(cqmPopHealthExchange).with(this.queueLoadPatientCqm).noargs();
	}
	
	private Map<String, Object> getFailureArguments() {
		Map<String, Object> arguments = new HashMap<>();
		return arguments;
	}
}
