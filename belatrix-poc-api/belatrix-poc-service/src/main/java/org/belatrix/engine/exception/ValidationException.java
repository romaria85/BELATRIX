package org.belatrix.engine.exception;

/**
 * Custom Validation Exception 
 * 
 * @author jalor
 *
 */
public class ValidationException extends IllegalStateException {

	private static final long serialVersionUID = 1L;
	
	
	public ValidationException() {
        super();
    }
    public ValidationException(String s) {
        super(s);
    }
    public ValidationException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public ValidationException(Throwable throwable) {
        super(throwable);
    }

}
