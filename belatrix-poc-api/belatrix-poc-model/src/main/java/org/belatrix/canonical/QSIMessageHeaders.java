package org.belatrix.canonical;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * 
 * @author jalor
 *
 */
public class QSIMessageHeaders implements Map<String, Object>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8876823267839935287L;

	private final Map<String, Object> headers;

	private static volatile IdGenerator idGenerator = null;
	private static final IdGenerator defaultIdGenerator = new AlternativeJdkIdGenerator();

	public QSIMessageHeaders(Map<String, Object> headers) {
		this.headers = (headers != null) ? new HashMap<String, Object>(headers) : new HashMap<String, Object>();
		IdGenerator generatorToUse = (idGenerator != null) ? idGenerator : defaultIdGenerator;
		this.headers.put("ID", generatorToUse.generateId());
	}

	public UUID getId() {
		return this.get("ID", UUID.class);
	}

	@Override
	public int size() {
		return this.headers.size();
	}

	@Override
	public boolean isEmpty() {
		return this.headers.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return this.headers.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return this.headers.containsValue(value);
	}

	@Override
	public Object get(Object key) {
		return this.headers.get(key);
	}

	@Override
	public Object put(String key, Object value) {
        return headers.put(key, value);
	}

	@Override
	public Object remove(Object key) {
		throw new UnsupportedOperationException("MessageHeaders is immutable.");
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		headers.putAll(m);
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("MessageHeaders is immutable.");
	}

	@Override
	public Set<String> keySet() {
		return Collections.unmodifiableSet(this.headers.keySet());
	}

	@Override
	public Collection<Object> values() {
		return Collections.unmodifiableCollection(this.headers.values());
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		return Collections.unmodifiableSet(this.headers.entrySet());
	}

	@Override
	public int hashCode() {
		return this.headers.hashCode();
	}

	@SuppressWarnings("unchecked")
	public <T> T get(Object key, Class<T> type) {
		Object value = this.headers.get(key);
		if (value == null) {
			return null;
		}
		if (!type.isAssignableFrom(value.getClass())) {
			throw new IllegalArgumentException("Incorrect type specified for header '" + key + "'. Expected [" + type
					+ "] but actual type is [" + value.getClass() + "]");
		}
		return (T) value;
	}

	public static interface IdGenerator {
		UUID generateId();
	}

	public static class AlternativeJdkIdGenerator implements IdGenerator {

		private final Random random;

		public AlternativeJdkIdGenerator() {
			SecureRandom secureRandom = new SecureRandom();
			byte[] seed = new byte[8];
			secureRandom.nextBytes(seed);
			this.random = new Random(new BigInteger(seed).longValue());
		}

		@Override
		public UUID generateId() {

			byte[] randomBytes = new byte[16];
			this.random.nextBytes(randomBytes);

			long mostSigBits = 0;
			for (int i = 0; i < 8; i++) {
				mostSigBits = (mostSigBits << 8) | (randomBytes[i] & 0xff);
			}
			long leastSigBits = 0;
			for (int i = 8; i < 16; i++) {
				leastSigBits = (leastSigBits << 8) | (randomBytes[i] & 0xff);
			}

			return new UUID(mostSigBits, leastSigBits);
		}

	}
	
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public Map<String, Object> getHeaders() {
		return headers;
	}
}
