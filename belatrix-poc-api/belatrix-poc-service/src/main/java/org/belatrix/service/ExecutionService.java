package org.belatrix.service;

import org.belatrix.model.Execution;

/**
 * 
 * @author jalor
 *
 */
public interface ExecutionService extends GenericService<Execution, String> {
	
}
