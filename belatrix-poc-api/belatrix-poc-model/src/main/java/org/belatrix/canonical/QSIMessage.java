package org.belatrix.canonical;

import java.util.Map;

public interface QSIMessage<T> {

	QSIMessageHeaders getHeaders();
	
	Map<String, Object> getGridPayload();
	
	void setGridPayload(Map<String, Object> gridPayload);
}
